
#include "JuceHeader.h"
#include "Window.h"
#include "AudioProcessing.h"
#include "DoubleBuffer.h"
#include "BitonalAnalizer.h"

#include <thread>
#include <mutex>
#include <fstream>

#define BUFFER_SIZE(n) 1 << n
#define SAMPLE_RATE	   22050

using namespace std;

class AudioRecognizer : public JUCEApplication
{
public:
	AudioRecognizer() {}

	const String getApplicationName() override { return ProjectInfo::projectName; }
	const String getApplicationVersion() override { return ProjectInfo::versionString; }
	bool moreThanOneInstanceAllowed() override { return true; }


	void initialise(const String& args) override
	{
		double_buffer = new DoubleBuffer();

		bool spectrogram = args.contains("-sp");

		fstream config_file("freq.conf");
		if (config_file.is_open()) {
			cout << "File di configurazione trovato" << endl;
			string line;
			while (getline(config_file, line)) {
				if (line.rfind("FREQ1=", 0) == 0) {
					config.freq1 = stof(line.substr(line.find("=") + 1));
				} else if (line.rfind("FREQ2=", 0) == 0) {
					config.freq2 = stof(line.substr(line.find("=") + 1));
				} else if (line.rfind("FREQ1T=", 0) == 0) {
					config.freq1_t = stof(line.substr(line.find("=") + 1));
				} else if (line.rfind("FREQ2T=", 0) == 0) {
					config.freq2_t = stof(line.substr(line.find("=") + 1));
				}
			}
			cout << "Configurazione caricata" << endl;
			config_file.close();
		} else {
			cout << "File di configurazione non trovato" << endl;
			return;
		}

		cout << "FREQ1  = " << config.freq1   << " Hz" << endl;
		cout << "FREQ1T = " << config.freq1_t << " s" << endl;
		cout << "FREQ2  = " << config.freq2   << " Hz" << endl;
		cout << "FREQ2T = " << config.freq2_t << " s" << endl;

		// police = new BitonalAnalizer(double_buffer, freq1_f, freq1_t_f, freq2_f, freq2_t_f, SAMPLE_RATE, FFT_SIZE);
		police = new BitonalAnalizer(double_buffer, config.freq1, config.freq1_t, config.freq2, config.freq2_t, SAMPLE_RATE, FFT_SIZE);
		audio_processing = new AudioProcessing(double_buffer, BUFFER_SIZE(6), SAMPLE_RATE);
		
		if (spectrogram) window = new Window(double_buffer);

		police.get()->start();

		if (spectrogram) window.get()->start();
	}

	void shutdown() override
	{
		double_buffer = nullptr;
		police = nullptr;
		window = nullptr;
		audio_processing = nullptr;
	}

	void systemRequestedQuit() override
	{
		quit();
	}

private:

	struct config {
		float freq1;
		float freq2;
		float freq1_t;
		float freq2_t;
	} config;

	ScopedPointer<DoubleBuffer> double_buffer;

	ScopedPointer<AudioProcessing> audio_processing;
	ScopedPointer<Window> window;

	ScopedPointer<BitonalAnalizer> police/*, ambulance, firefighters*/;
};

// This macro generates the main() routine that launches the app.
START_JUCE_APPLICATION(AudioRecognizer);
